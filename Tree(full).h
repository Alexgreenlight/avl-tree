//---------------------------------------------------------------------------

#ifndef TreeH
#define TreeH

#include <string.h>
#include <stdio.h>
#include <iostream.h>
#include <stack>
//---------------------------------------------------------------------------
template<class T>
class tree;
template<class T>
class Iterator;

template<class T>
class node{
	friend class tree<T>;
	friend class Iterator<T>;
	char key[64];
	unsigned int h;
	T data;
	node* left;
	node* right;
	node* prev;

	public:
	node(node* pr,char* k, const T dt){
	data=dt;
	strcpy(key,k);
    left=right=0;
    prev=pr;
	h=1;
    }
};

template<class T>
class tree;

template<class T>
class Iterator{
		friend class tree<T>;
		stack<node<T>*> S;
		node<T> *top;
		node<T> *cur;
		public:
		Iterator(){ top=0; cur=0; }

		Iterator(tree<T>& t){
		unsigned int ss = t.getsize();
		if (ss>0) {
		   S.push(top);
		}
		top=t.top; cur=top;  }

		void init( tree<T>& t) {
		unsigned int ss = t.getsize();
		if (ss>0) {
		   S.push(top);
		} top=t.top; cur=top; }

		void begin(){ cur=top;
		while (!S.empty()) S.pop();
		S.push(top); }

		void clear(){ cur=top=0;
		while (!S.empty()) S.pop(); }

		bool onend(){ return S.empty(); }

		bool operator++(int){
		   if (this->onend()) {
				 return false;
		   }
		   if (cur->right!=0) {
			   S.push(cur->right);
		   }
		   if (cur->left!=0) {
			   cur=cur->left;
		   }
		   else{
				cur=S.top();
				S.pop();
		   }
		   return true;
		}
		T get(){
			if (!cur) {
               return NULL;
			}
			return cur->data;
		}

};

template<class T>
class tree{
	friend class node<T>;
	friend class Iterator<T>;
	node<T> *top; //������ �� ������
	unsigned int size; //���-�� ��������� � ������
	int bfactor(node<T>* p); //������� ���� ����������� ����
	void recalch(node<T>* p); //���������� ������ ������ � ����
	node<T>* rightrot(node<T>* p); //������ ������� ������ ����
	node<T>* leftrot(node<T>* p);  //����� ������� ������ ����
	unsigned int height(node<T>* p); //������ ������ � ����
	node<T>* dobalance(node<T>* p); //������������ ������ � ����
	node<T>* findmin(node<T>* p);
	node<T>* removemin(node<T>* p);
	node<T>* add(node<T>* from, node<T>* p, char* k, const T& dt); //���������� ����
	node<T>* remove(node<T>* p, char* k);  //�������� ���� �� �����
	void cl(node<T>* p);  //�������� ����� ������
	node<T>* findme(node<T>* p, char* k); //����� �� ����� � ������
	void up_down(node<T>* p, int l); //����� ������ ������ ����
public:
	tree(); //�����������
	tree(char* k, const T& dt); //�����������-�������������
	~tree(); //����������
	void clear(); //������� ������
	unsigned int getheight(); //������ ������ ������
	void addnode(char* k, const T dt);  //�������� ���� � ������
	void removenode(char* k);  //������� �� �����
	node<T>* find(char* k); //����� �� ����� � ������� ������
	unsigned int getsize(); //�������� ����� ���������� ���������
	void print();  //���������� ������ (�.�. ����� �������� ������)
};

template<class T>
tree<T>::tree(){
	top=0;
	size=0;
}

template<class T>
tree<T>::tree(char* k, const T& dt){
	 top = new node(0,k,dt);
	 size=1;
}

template<class T>
tree<T>::~tree(){
	if (top) {
	   this->clear();
	}
}

template<class T>
void tree<T>::clear(){
	if (!top){
	   return;
	}
	this->cl(top);
	top=0;
	size=0;
	return;
}

template<class T>
void tree<T>::cl(node<T>* p){
	if (p->right || p->left){
		if (p->right) {
		 node<T>* rt = p->right;
		 cl(rt);
		}
		if (p->left) {
		 node<T>* lt = p->left;
		cl(lt);
		}

	}
	delete p;
	return;
}

template<class T>
unsigned int tree<T>::getheight(){
	if (!top) {
		return 0;
	}
	else {
		return top->h;
	}
}

template<class T>
unsigned int tree<T>::getsize(){
	return this->size;
}

template<class T>
unsigned int tree<T>::height(node<T>* p){
if (!p){
	return 0; }
else{
	return p->h;
}
}

template<class T>
int tree<T>::bfactor(node<T>* p){
	if (!p) return -10000;
	else return height(p->right)-height(p->left);
}

template<class T>
void tree<T>::recalch(node<T>* p){
	unsigned int hl=height(p->left);
    unsigned int hr=height(p->right);
    if (hl>hr){
		p->h=hl+1;
	}
    else{
        p->h=hr+1;
    }
}

template<class T>
node<T>* tree<T>::rightrot(node<T>* p){
	node<T>* n=p->left;
    n->prev=p->prev;
	p->prev=n;
	if (n->right)
	n->right->prev=p;
    p->left=n->right;
	n->right=p;
	recalch(p);
    recalch(n);
    return n;
}

template<class T>
node<T>* tree<T>::leftrot(node<T>* p){
	node<T>* n = p->right;
    n->prev=p->prev;
	p->right=n->left;
	if (p->right)
    p->right->prev=p;
    n->left=p;
    p->prev=n;
	recalch(p);
	recalch(n);
    return n;
}

template<class T>
node<T>* tree<T>::dobalance(node<T>* p){
	recalch(p);
    if (bfactor(p)==2){
        if (bfactor(p->right)<0)
            p->right=rightrot(p->right);
        return leftrot(p);
	}
    if (bfactor(p)==-2){
        if (bfactor(p->left)>0)
            p->left=leftrot(p->left);
        return rightrot(p);
    }
	return p;
}

template<class T>
node<T>* tree<T>::add(node<T> *from, node<T>* p, char* k, const T& dt){
	if (!p) return new node<T>(from,k,dt);
	if (strcmp(k,p->key)>0)
		p->left=add(p,p->left,k,dt);
	else
		p->right=add(p,p->right,k,dt);
    return dobalance(p);
}

template<class T>
void tree<T>::addnode(char* k, const T dt){
	top=add(0,top,k,dt);
	size++;
	return;
}

template<class T>
node<T>* tree<T>::findmin(node<T>* p){
	if (p->left)
		return findmin(p->left);
	else return p;
}

template<class T>
node<T>* tree<T>::removemin(node<T>* p){
	if (p->left==0){
		return p->right;
        }
    p->left=removemin(p->left);
	return dobalance(p);
}

template<class T>
node<T>* tree<T>::remove(node<T>* p, char* k){
	if (!p) return 0;
	if (strcmp(k,p->key)>0)
		p->left=remove(p->left,k);
	else if (strcmp(k,p->key)<0)
		p->right=remove(p->right,k);
	else{
		node<T>* lt=p->left;
		node<T>* rt=p->right;
        node<T>* pr=p->prev;
		delete p;
		if (!rt) {
		if (lt)
        lt->prev=pr;
		return lt; }
		node<T>* min=findmin(rt);
		min->right=removemin(rt);
		if (min->right)
		min->right->prev=min;
		min->left=lt;
		if (lt)
		lt->prev=min;
		return dobalance(min);
	}
	size--;
	return dobalance(p);
}

template<class T>
void tree<T>::removenode(char* k){
	top=remove(top,k);
}


template<class T>
node<T>* tree<T>::find(char* k){
	if (!top) {
	   printf("������ ������!");
	   return NULL;
	}
	node<T>* rez = findme(top,k);
	return rez;
}

template<class T>
node<T>* tree<T>::findme(node<T>* p, char* k){
	while ( p ){
		if ( strcmp(k,p->key)==0 )
		 break;
		if ( strcmp(k,p->key)>0 ){
		 p=p->left;
         continue; }
		if ( strcmp(k,p->key)<0 )
		 p=p->right;
	}
	if(!p){ printf("no ");
	return NULL; }
	else{ printf("yes ");
	return p; }
}

template<class T>
void tree<T>::print(){
	node<T>* pos = this->top;
	if ( !pos ){
		printf("������ ������\n");
		return;
    }
	printf("������:\n");
	up_down(pos,0);
}

template<class T>
void tree<T>::up_down(node<T>* p, int l){
	if ( !p ) return;
	cout<<"������� "<<l<<", ������ "<<p->data<<endl;
	up_down(p->left,l+1);
	up_down(p->right,l+1);
}
#endif





